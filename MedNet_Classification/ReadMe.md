
###  Medical Image Classification model (MedNet V2) Using the MedNIST Dataset

#### _This project is an optimised version of the [MedNet model V1](https://github.com/apolanco3225/Medical-MNIST-Classification.git)._

In this version of MedNet, we had changed the hyperparameters and the architecture of the used model in [V1](https://github.com/apolanco3225/Medical-MNIST-Classification.git), by:
- Changing the kernel size of CNN layers into (3,3)
- Changing the number of out channels in first layer into 12, and 20 in the seconde CNN layer
- Applying BatchNorm2D after each CNN layer
- Adding MaxPool2d after the first CNN layer with (2,2) kernel size
