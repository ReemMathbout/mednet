##  Medical Image Classification Interface (MedNet App) Using the MedNIST Dataset

####  This project is heavily inspired by [MedNet App V1](https://github.com/BenhajjiNoura/mednist-classification.git).
 Where in this version the user can import different X Ray images, and classify them automatically in the suitable folder due to the trained model MedNet V2.

### _Requirements_

Install them from `requirements.txt`:

    pip install -r requirements.txt


###  _Local Deployment_

Run the server:

    python app.py


### License

The mighty MIT license. Please check `LICENSE` for more details.
