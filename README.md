# MedNet

This project contains:
- MedNet classification: which contains the notebooks to train CNN model using Pytorch, which classify Xray images into six categories
- MedNet App: interface to do inference with the trained model in MedNet classification
