#### ***To be able to use this package, follow the steps:***

1. First create new conda environment
  ```shell script
   conda create -n environment_name python=3.6 
  ```
2. Activate the new created conda environment by:
    ```shell script
    conda activate environment_name
    ```
3. Clone the project from the repo https://gitlab.com/ReemMathbout/mednet.git 

4. Access to the folder where you cloned the project into
5. After that, execute the following commands to install the required packages:
    ```shell script
    pip install -r requirements.txt

    ```